name := "cats-effect-exercises"
version := "0.1"
scalaVersion := "2.13.13"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "2.6.0",
  "org.typelevel" %% "cats-effect" % "2.5.3"
)

scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.5.0"

scalacOptions ++= Seq(
"-Xfatal-warnings",
"-deprecation"
)

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.13.3" cross CrossVersion.full)
