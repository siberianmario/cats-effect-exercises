package info.siberianmario.cats.effect

import cats.effect._
import cats.effect.concurrent._
import cats.effect.implicits._
import cats.implicits._
import cats.{Foldable, Parallel, Traverse}
import info.siberianmario.cats.effect.Nursery.State

import scala.concurrent.duration._

trait Nursery[F[_]] {
  def startSoon[A](fa: F[A]): F[Fiber[F, A]]
}

class NurseryImpl[F[_]: Concurrent](
  private val state: MVar2[F, State[F]],
  private val counter: Ref[F, Long],
  private val result: TryableDeferred[F, Either[Throwable, Unit]]
) extends Nursery[F] {

  def putStr(str: String): F[Unit] = Sync[F].delay(println(str))

  override def startSoon[A](fa: F[A]): F[Fiber[F, A]] = {
    safeModify { s =>
      for {
        n <- counter.getAndUpdate(_ + 1)
        fiber <- fa.guaranteeCase {
          case ExitCase.Canceled => removeFiber(n)
          case _ => ().pure[F]
        }.start
        newFibers = s.fibers + (n -> fiber.asInstanceOf[Fiber[F, Any]])
        _ <- putStr(s"added fiber $n")
      } yield (s.copy(fibers = newFibers), fiber)
    }
  }

  def removeFiber(n: Long): F[Unit] =
    result.tryGet.flatMap { d =>
      safeModify_ { s =>
        s.copy(fibers = s.fibers - n).pure[F] <* putStr(s"removed fiber $n")
      }.whenA(d.isEmpty)
    }

  def safeModify[B](f: State[F] => F[(State[F], B)]): F[B] =
    state.modify { s =>
      for {
        _ <- s.completer.traverse(_.cancel)
        optResult <- result.tryGet
        newState <- optResult match {
          case Some(Left(ex)) => ex.raiseError[F, (State[F], B)]
          case Some(_) => new Exception("Out of Nursery scope").raiseError[F, (State[F], B)]
          case None => f(s).flatMap(_.leftTraverse(updateCompleter))
        }
      } yield newState
    }.uncancelable

  def safeModify_(f: State[F] => F[State[F]]): F[Unit] =
    safeModify(s => f(s).map((_, ())))

  def updateCompleter(s: State[F]): F[State[F]] =
    waitAll(s.fibers.values.toList)
      .attempt
      .flatMap { r =>
        putStr(s"completing with $r") >> result.complete(r)
      }
      .start
      .map { completer =>
        s.copy(completer = completer.some)
      } <* putStr(s"completer updated, tracking ${s.fibers.size} fibers")

  def waitAll[G[_]: Foldable, B](fs: G[Fiber[F, B]]): F[Unit] =
    fs.foldLeft(().pure[F]) { (res, fiber) =>
      (res racePair fiber.join.void).flatMap {
        case Left((_, fb)) => fb.join
        case Right((fb, _)) => fb.join
      }
    }

  def getResult: F[Unit] =
    putStr("waiting for fibers") >> result.get.rethrow

  def unsafeCancelFibers(): F[Unit] =
    state.read.flatMap { s =>
      s.completer.traverse_(_.cancel) >>
        result.complete(().asRight).attempt >>
        s.fibers.values.toList.traverse_(_.cancel) >>
        putStr(s"cancelled ${s.fibers.size} fibers")
    }
}

object Nursery {

  type Fibers[F[_]] = Map[Long, Fiber[F, Any]]

  case class State[F[_]](fibers: Fibers[F], completer: Option[Fiber[F, Unit]])

  // Implement this. You need `Concurrent` since you'd be using `.start` internally,
  // and you should NOT need anything else
  def apply[F[_]: Concurrent]: Resource[F, Nursery[F]] = {
    val resource = for {
      guard <- Deferred[F, Unit]
      deferred <- Deferred.tryable[F, Either[Throwable, Unit]]
      counter <- Ref[F].of(0L)
      state <- MVar[F].of(State[F](Map.empty, None))
      nursery = new NurseryImpl[F](state, counter, deferred)
      _ <- nursery.startSoon(guard.get)
    } yield Resource.makeCase(nursery.pure[F]) {
      case (n, ExitCase.Completed) =>
        guard.complete(()) >>
          n.getResult.guarantee {
            n.unsafeCancelFibers()
          }
      case (n, _) =>
        n.unsafeCancelFibers()
    }

    Resource.suspend(resource)
  }
}

object Main2 extends IOApp {
  def concSequence[F[_]: Concurrent, G[_]: Traverse, A](gfa: G[F[A]]): F[G[A]] =
    Nursery[F].use { n =>
      gfa.traverse(n.startSoon).flatMap(_.traverse(_.join))
    }

  override def run(args: List[String]): IO[ExitCode] =
    happyPath[IO] >>
      nestedUsage[IO] >>
      manualCancelWorks[IO] >>
      errorsCancelOthers[IO] >>
      failToStartAfterError[IO] >>
      cancellationIsPropagated[IO] >>
      parSequenceParity[IO] >>
      IO(println("Run completed!")) >>
      IO.pure(ExitCode.Success)


  def happyPath[F[_]: Concurrent]: F[Unit] = test[F] { n =>
    for {
      d1 <- Deferred[F, Unit]
      d2 <- Deferred[F, Unit]
      _ <- n.startSoon(d1.get)
      _ <- n.startSoon(d2.get)
      _ <- n.startSoon(d1.complete(()))
      _ <- n.startSoon(d2.complete(()))
      _ <- n.startSoon(().pure[F])
      _ <- n.startSoon(().pure[F])
      _ <- n.startSoon(().pure[F])
      _ <- n.startSoon(().pure[F])
    } yield ()
  }

  def nestedUsage[F[_]: Concurrent]: F[Unit] = test[F] { n =>
    n.startSoon {
      for {
        d1 <- Deferred[F, Unit]
        d2 <- Deferred[F, Unit]
        d3 <- Deferred[F, Unit]
        _ <- n.startSoon(d1.get >> n.startSoon(d2.complete(())))
        _ <- n.startSoon {
          n.startSoon(d1.complete(())) >>
            n.startSoon(d2.get) >>
            n.startSoon(d3.complete(()))
        }
        _ <- d3.get
      } yield ()
    }.void
  }

  def manualCancelWorks[F[_]: Concurrent: Timer]: F[Unit] = test[F] { n =>
    for {
      d <- Deferred[F, Unit]
      f <- n.startSoon(Timer[F].sleep(1.second).guaranteeCase {
        case ExitCase.Canceled => d.complete(())
        case _ => new Exception().raiseError[F, Unit]
      })
      _ <- f.cancel
      _ <- d.get
    } yield ()
  }

  def errorsCancelOthers[F[_]: Concurrent: Timer]: F[Unit] = test[F] { n =>
    for {
      d <- Deferred[F, Unit]
      _ <- n.startSoon(Concurrent[F].never[Unit].guaranteeCase {
        case ExitCase.Canceled => d.complete(())
        case _ => new Exception("Expected cancellation").raiseError[F, Unit]
      })
      _ <- n.startSoon(ExpectedException.raiseError[F, Unit])
      //      _ <- d.get
    } yield ()
  }

  def failToStartAfterError[F[_]: Concurrent: Timer]: F[Unit] = test[F] { n =>
    for {
      d1 <- Deferred[F, Unit]
      d2 <- Deferred[F, Unit]
      _ <- n.startSoon(d1.complete(()) >> ExpectedException.raiseError[F, Unit])
      _ <- d1.get
      _ <- Timer[F].sleep(1.second)
      _ <- n.startSoon(Concurrent[F].never[Unit]).attempt.flatMap {
        case Left(ExpectedException) => d2.complete(())
        case _ => ().pure[F]
      }
      _ <- d2.get
    } yield ()
  }

  def cancellationIsPropagated[F[_]: Concurrent]: F[Unit] = {
    for {
      d1 <- Deferred[F, Unit]
      d2 <- Deferred[F, Unit]
      d3 <- Deferred[F, Unit]
      f <- test[F] { n =>
        n.startSoon(d1.complete(()) >> Concurrent[F].never[Unit].onCancel(d3.complete(()))) >>
          d2.get
      }.start
      _ <- d1.get
      _ <- f.cancel
      _ <- d2.complete(())
      _ <- d3.get
    } yield ()
  }

  def parSequenceParity[F[_]: Concurrent: Timer: Parallel]: F[Unit] = Ref[F].of(Map.empty[String, Int]).flatMap { log =>
    val crash = Timer[F].sleep(350.millis) >> new Exception().raiseError[F, Unit]
    val jobs =
      (crash :: (100 to 500 by 100).map(_.millis).map(Timer[F].sleep).toList).map { job =>
        job.guaranteeCase(ec => log.update { map =>
          map |+| Map(ec.getClass.getSimpleName -> 1)
        })
      }

    for {
      _ <- jobs.parSequence.attempt
      parLog <- log.getAndSet(Map())
      _ <- concSequence(jobs).attempt
      concLog <- log.getAndSet(Map())
      _ <- Sync[F].delay {
        println(s"parLog = $parLog")
        println(s"concLog = $concLog")
        require(parLog == concLog)
      }
    } yield ()
  }

  def trackedNursery[F[_]: Concurrent](ref: Ref[F, Int]): Resource[F, Nursery[F]] =
    Nursery[F].map { n =>
      new Nursery[F] {
        override def startSoon[A](fa: F[A]): F[Fiber[F, A]] =
          n.startSoon(ref.update(_ + 1).bracket(_ => fa)(_ => ref.update(_ - 1)))
      }
    }

  object ExpectedException extends Exception

  def test[F[_]: Concurrent](run: Nursery[F] => F[Unit]): F[Unit] =
    Ref[F].of(0).flatMap { active =>
      trackedNursery(active).use(run).attempt <* active.get.flatMap {
        case 0 => ().pure[F]
        case other => new Exception(s"Active fiber count is $other, expected 0").raiseError[F, Unit]
      }
    }.rethrow.recover { case ExpectedException => () }
}