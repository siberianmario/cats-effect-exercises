package info.siberianmario.cats.effect

import scala.concurrent.duration._

import cats.effect._
import cats.effect.concurrent._
import cats.effect.implicits._
import cats.implicits._

sealed trait Scope[F[_]] {
  def open[A](ra: Resource[F, A]): F[A]
}

object Scope {
  // Implement this. Add context bounds as necessary.
  def apply[F[_]: Concurrent]: Resource[F, Scope[F]] = {
    val allocate = for {
      releaseM <- MVar[F].of(().pure[F])
      scope = new Scope[F] {
        def open[A](ra: Resource[F, A]): F[A] =
          releaseM.modify { release =>
            ra.allocated.map { case (a, releaseA) =>
              (releaseA.guarantee(release), a)
            }
          }.uncancelable

//        def open[A](ra: Resource[F, A]): F[A] =
//          releaseM.take.flatMap { release =>
//            ra.allocated.attempt.flatMap {
//              case Right((a, releaseA)) =>
//                releaseM.put(releaseA.guarantee(release)) >> a.pure[F]
//              case Left(ex) =>
//                releaseM.put(release) >> ex.raiseError[F, A]
//            }
//          }.uncancelable
      }
    } yield (scope, releaseM.take.flatten)

    Resource(allocate)
  }
}

object Main extends IOApp {

  case class TestResource(idx: Int)

  override def run(args: List[String]): IO[ExitCode] = {
    happyPath[IO] >>
      atomicity[IO] >>
      cancelability[IO] >>
      errorInRelease[IO] >>
      errorInAcquire[IO] >>
      IO(println("Run completed")) >>
      IO.pure(ExitCode.Success)
  }

  case class Allocs[F[_]](
    normal: Resource[F, TestResource],
    slowAcquisition: Resource[F, TestResource],
    crashOpen: Resource[F, TestResource],
    crashClose: Resource[F, TestResource],
  )

  def happyPath[F[_] : Concurrent : Timer] =
    test[F]("happyPath") { (allocs, scope, _) =>
      for {
        r1 <- scope.open(allocs.normal)
        r2 <- scope.open(allocs.slowAcquisition)
        r3 <- scope.open(allocs.normal)
      } yield ()
    } { (allocs, deallocs, ec) =>
      Sync[F].delay {
        require(allocs == Vector(1, 2, 3))
        require(allocs == deallocs.reverse)
        require(ec == ExitCase.Completed)
      }
    }

  def atomicity[F[_] : Concurrent : Timer] =
    test[F]("atomicity") { (allocs, scope, cancelMe) =>
      for {
        r1 <- scope.open(allocs.normal)
        lock <- Deferred[F, Unit]
        _ <- (lock.get >> cancelMe).start
        r2 <- scope.open(Resource.eval(lock.complete(())) >> allocs.slowAcquisition)
        _ <- Timer[F].sleep(1.second)
        r3 <- scope.open(allocs.normal)
      } yield ()
    } { (allocs, deallocs, ec) =>
      Sync[F].delay {
        require(allocs == Vector(1, 2))
        require(deallocs == Vector(2, 1))
        require(ec == ExitCase.Canceled)
      }
    }

  def cancelability[F[_] : Concurrent : Timer] =
    test[F]("cancelability") { (allocs, scope, cancelMe) =>
      for {
        r1 <- scope.open(allocs.normal)
        r2 <- scope.open(allocs.slowAcquisition)
        _ <- cancelMe
        _ <- Timer[F].sleep(1.second)
        r3 <- scope.open(allocs.normal)
      } yield ()
    } { (allocs, deallocs, ec) =>
      Sync[F].delay {
        require(allocs == Vector(1, 2))
        require(deallocs == Vector(2, 1))
        require(ec == ExitCase.Canceled)
      }
    }

  def errorInRelease[F[_] : Concurrent : Timer] =
    test[F]("errorInRelease") { (allocs, scope, _) =>
      for {
        r1 <- scope.open(allocs.normal)
        r2 <- scope.open(allocs.crashClose)
        r3 <- scope.open(allocs.normal)
      } yield ()
    } { (allocs, deallocs, ec) =>
      Sync[F].delay {
        require(allocs == Vector(1, 2, 3))
        require(deallocs == Vector(3, 1))
        require(ec match {
          case ExitCase.Error(_) => true
          case _ => false
        })
      }
    }

  def errorInAcquire[F[_] : Concurrent : Timer] =
    test[F]("errorInAcquire") { (allocs, scope, _) =>
      for {
        r1 <- scope.open(allocs.normal)
        r2 <- scope.open(allocs.crashOpen)
        r3 <- scope.open(allocs.normal)
      } yield ()
    } { (allocs, deallocs, ec) =>
      Sync[F].delay {
        require(allocs == Vector(1))
        require(deallocs == Vector(1))
        require(ec match {
          case ExitCase.Error(_) => true
          case _ => false
        })
      }
    }


  def test[F[_] : Concurrent : Timer](
    name: String)(
    run: (Allocs[F], Scope[F], F[Unit]) => F[Unit])(
    check: (Vector[Int], Vector[Int], ExitCase[Throwable]) => F[Unit]
  ): F[Unit] =
    for {
      idx <- Ref[F].of(1)
      allocLog <- Ref[F].of(Vector.empty[Int])
      deallocLog <- Ref[F].of(Vector.empty[Int])
      open = idx.modify(i => (i + 1, i))
        .flatTap(i => allocLog.update(_ :+ i)).map(TestResource)
      close = (r: TestResource) => deallocLog.update(_ :+ r.idx)
      cancel <- Deferred[F, Unit]
      slow = Timer[F].sleep(1.second)
      allocs = Allocs[F](
        Resource.make(open)(close),
        Resource.make(slow >> open)(close),
        Resource.make(Sync[F].raiseError(new Exception))(close),
        Resource.make(open)(_ => Sync[F].raiseError(new Exception))
      )
      finish <- Deferred[F, Either[Throwable, Unit]]
      scope = for {
        _ <- Resource.makeCase(().pure[F])((_, ec) => for {
          allocs <- allocLog.get
          deallocs <- deallocLog.get
          _ <- Sync[F].delay(println(s"$name: allocations = $allocs, deallocations = $deallocs, ExitCode = $ec"))
          result <- check(allocs, deallocs, ec).attempt
          _ <- finish.complete(result)
        } yield ())
        s <- Scope[F]
      } yield s
      _ <- scope.use(run(allocs, _, cancel.complete(())))
        .race(cancel.get)
        .attempt
      _ <- finish.get.rethrow
    } yield ()
}