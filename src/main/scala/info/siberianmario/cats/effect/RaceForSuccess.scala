package info.siberianmario.cats.effect

import cats.Traverse
import cats.data.NonEmptyList
import cats.effect.IOApp.Simple
import cats.effect.implicits._
import cats.effect.{Concurrent, ExitCase, Fiber, IO, Resource, Timer}
import cats.implicits._

import scala.concurrent.duration._
import scala.util.Random

object RaceForSuccess extends Simple {

  case class Data(source: String, body: String)

  def provider(name: String)(implicit timer: Timer[IO]): IO[Data] = {
    val proc = for {
      dur <- IO(Random.between(1, 5).seconds)
      _   <- IO(println(s"$name request started ($dur)"))
      _   <- IO.sleep(dur)
      _   <- IO(if (Random.nextDouble() < 0.8) throw new Exception(s"Error in $name"))
      txt <- IO(Random.alphanumeric.take(16).mkString)
    } yield Data(name, txt)

    proc.guaranteeCase {
      case ExitCase.Completed => IO(println(s"$name request finished"))
      case ExitCase.Canceled  => IO(println(s"$name request canceled"))
      case ExitCase.Error(_)  => IO(println(s"$name errored"))
    }
  }

  // Use this class for reporting all failures.
  case class CompositeException (ex: NonEmptyList[Throwable]) extends Exception("All race candidates have failed")

  def startR[F[_]: Concurrent, A](fa: F[A]): Resource[F, Fiber[F, A]] = Resource(fa.start.fproduct(_.cancel))

  // Implement this function:
  def raceToSuccess[F[_]: Concurrent, G[_]: Traverse, A](gfa: G[F[A]]): F[A] =
    gfa.traverse(startR[F, A]).use { fibers =>
      fibers
        .map(_.join.attempt.map(_.toEitherNel))
        .reduceLeftOption { case (lh, rh) =>
          (lh racePair rh).flatMap {
            case Left((Left(ex), fb2))  => fb2.join.map(_.leftMap(ex ::: _))
            case Right((fb1, Left(ex))) => fb1.join.map(_.leftMap(_ ::: ex))
            case Left((Right(a), _))    => a.asRight[NonEmptyList[Throwable]].pure[F]
            case Right((_, Right(b)))   => b.asRight[NonEmptyList[Throwable]].pure[F]
          }
        }
        .fold(new UnsupportedOperationException("empty traversable").raiseError[F, A]) { fa =>
          fa.map(_.leftMap(CompositeException)).rethrow
        }
    }

  // In your IOApp, you can use the following sample method list

  val methods: List[IO[Data]] = List(
    "memcached",
    "redis",
    "postgres",
    "mongodb",
    "hdd",
    "aws"
  ).map(provider)

  override def run: IO[Unit] =
    raceToSuccess(methods)
      .attemptNarrow[CompositeException]
      .flatMap {
        case Right(Data(src, body)) => IO(println(s"$src: $body"))
        case Left(ex@CompositeException(nel)) =>
          val errors = nel.map(_.getMessage).mkString_(", ")
          IO(println(s"${ex.getMessage}: $errors"))
      }
//      .start
//      .flatMap { fiber =>
//        IO.sleep(100.millis) >> fiber.cancel
//      }
}
