package info.siberianmario.cats.effect

import cats.effect.IOApp.Simple
import cats.effect.concurrent.{Ref, Semaphore}
import cats.effect.{Async, IO, Sync}
import cats.syntax.all._

object Memoization extends Simple {

  def memoize[F[_]: Sync, A](fa: F[A]): F[F[A]] =
    for {
      ref <- Ref[F].of(fa.attempt)
      _ <- ref.update(_.flatTap(a => ref.set(a.pure[F])))
    } yield ref.get.flatten.rethrow

  def safeMemoize[F[_]: Async, A](fa: F[A]): F[F[A]] =
    for {
      sem <- Semaphore.uncancelable[F](1)
      mem <- memoize(fa)
    } yield sem.withPermit(mem)

  override def run: IO[Unit] = {
    object Boo extends Exception("Boo!")

    val program: IO[String] = IO {
      println("Executing..."); throw Boo
    }

    for {
      memo <- safeMemoize(program)
      boos <- List.fill(8)(memo.handleError { case Boo => "Got Boo" }).parSequence
      _ <- IO(println(boos))
    } yield ()
  }
}
