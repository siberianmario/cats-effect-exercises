package info.siberianmario.cats.effect

import cats.effect.concurrent.{MVar, MVar2, Ref}
import cats.effect.implicits._
import cats.effect.{BracketThrow, Concurrent, ExitCode, IO, IOApp, Sync, Timer}
import cats.implicits._

import scala.concurrent.duration._
import scala.util.Random

object Worker extends IOApp {

  // To start, our requests can be modelled as simple functions.
  // You might want to replace this type with a class if you go for bonuses. Or not.
  type Worker[F[_], A, B] = A => F[B]

  // Sample stateful worker that keeps count of requests it has accepted
  def mkWorker[F[_]: Sync: Timer](id: Int): F[Worker[F, Int, Int]] =
    Ref[F].of(0).map { counter =>
      def simulateWork: F[Unit] =
        Sync[F].delay(50 + Random.nextInt(450)).map(_.millis).flatMap(Timer[F].sleep)

      def report: F[Unit] =
        counter.get.flatMap(i => Sync[F].delay(println(s"Total processed by $id: $i")))

      x =>
        simulateWork >>
          counter.update(_ + 1) >>
          report >>
          Sync[F].pure(x + 1)
    }

  trait WorkerPool[F[_], A, B] {
    def exec(a: A): F[B]

    def addWorker(w: Worker[F, A, B]): F[Unit]

    def removeAllWorkers(): F[Unit]
  }

  object WorkerPool {
    // Implement this constructor, and, correspondingly, the interface above.
    // You are free to use named or anonymous classes
    def of[F[_]: Concurrent: BracketThrow, A, B](fs: List[Worker[F, A, B]]): F[WorkerPool[F, A, B]] = {
      for {
        queue <- MVar.empty[F, Worker[F, A, B]]
        refQueue <- Ref.of[F, MVar2[F, Worker[F, A, B]]](queue)
        _ <- fs.traverse(w => queue.put(w).start)
      } yield new WorkerPool[F, A, B] {
        override def exec(a: A): F[B] =
          for {
            q <- refQueue.get
            w <- q.take
            b <- w(a).guarantee(q.put(w).start.void)
          } yield b

        override def addWorker(w: Worker[F, A, B]): F[Unit] =
          refQueue.get.flatMap(_.put(w))

        override def removeAllWorkers(): F[Unit] =
          MVar.empty[F, Worker[F, A, B]].flatMap(refQueue.set)
      }
    }
  }

  // Sample test pool to play with in IOApp
  val testPool: IO[WorkerPool[IO, Int, Int]] =
    List.range(0, 4)
      .traverse(mkWorker[IO])
      .flatMap(WorkerPool.of[IO, Int, Int])


  override def run(args: List[String]): IO[ExitCode] =
    for {
      pool <- testPool
      _ <- IO(println("submitting 10 tasks"))
      results <- List.range(0, 10).traverse(pool.exec)
      _ <- IO(println(s"results = $results"))
      _ <- IO(println("submitting 10 tasks and sleep for 1 sec"))
      _ <- List.range(10, 20).traverse(pool.exec).flatMap(res => IO(println(s"results = $res"))).start
      _ <- IO.sleep(1.second)
      _ <- IO(println("removing all workers and sleep for 2 sec"))
      _ <- pool.removeAllWorkers()
      _ <- IO.sleep(2.seconds)
      _ <- IO(println("adding two workers"))
      workers <- List(4, 5).traverse(mkWorker[IO])
      _ <- workers.traverse_(pool.addWorker)
      _ <- IO.sleep(5.seconds)
    } yield ExitCode.Success
}
